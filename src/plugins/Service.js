import axios from 'axios';

export default {
    install: (Vue) => {
        const Service = {
            get (url) {
                //console.log('IMPRIMIENDO DESDE PLUGIN SERVICE');
                const init = {
                    method: 'GET',
                    url
                }
                return axios(init);
            }
        }
        Vue.prototype.$service = Service;
    }
};